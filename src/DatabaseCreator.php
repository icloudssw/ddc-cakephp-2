<?php

class DatabaseCreator {
	public function create($actualConfigs, $newConfigs){

		if(empty($newConfigs)){
			$newConfigs = [
				// 'name' => 'ortosystem_clinic_122',
				'datasource' => 'Database/Mysql',
				'persistent' => 'false',
				'host' => 'localhost',
				'login' => 'root',
				'password' => 'root',
				'schema' => null,
				'prefix' => null,
				'encoding' => null,
				'port' => null
				// 'database' => 'ortosystem_clinic_122',
			];
		}

		$name = 'orosystem_clinic_' . time(); // pega isso se nao for setado um name
		$newConfigs['name'] = !empty($newConfigs['name']) ? $newConfigs['name'] : $name;
		$newConfigs['database'] = !empty($newConfigs['name']) ? $newConfigs['name'] : $name;


		$this->path = APP . 'Config' . DS;

		$filename = $this->path . 'database.php';

		$out = "<?php\n";
		$out .= "class DATABASE_CONFIG {\n\n";
		if (!empty($datasource) && strpos($datasource, 'Database/') === false) {
			$datasource = "Database/{$datasource}";
		}

		foreach ($actualConfigs as $key => $config) {

			extract($config);
			$name = $key;
			$out .= "\tpublic \${$name} = array(\n";
			$out .= "\t\t'datasource' => '{$datasource}',\n";
			$out .= "\t\t'persistent' => false,\n";
			$out .= "\t\t'host' => '{$host}',\n";
			if (!empty($port) && $port) {
				$out .= "\t\t'port' => {$port},\n";
			}
			$out .= "\t\t'login' => '{$login}',\n";
			$out .= "\t\t'password' => '{$password}',\n";
			$out .= "\t\t'database' => '{$database}',\n";
			if (!empty($schema) && $schema) {
				$out .= "\t\t'schema' => '{$schema}',\n";
			}else{
				$out .= "\t\t'schema' => null,\n";
			}
			if (!empty($prefix) && $prefix) {
				$out .= "\t\t'prefix' => '{$prefix}',\n";
			}else{
				$out .= "\t\t'prefix' => null,\n";
			}

			$out .= "\t\t'encoding' => 'utf8',\n";
			$out .= "\t);\n";
		}
		extract($newConfigs);

		$out .= "\tpublic \${$name} = array(\n";
		$out .= "\t\t'datasource' => '{$datasource}',\n";
		$out .= "\t\t'persistent' => false,\n";
		$out .= "\t\t'host' => '{$host}',\n";

		if (isset($port)) {
			$out .= "\t\t'port' => {$port},\n";
		}

		$out .= "\t\t'login' => '{$login}',\n";
		$out .= "\t\t'password' => '{$password}',\n";
		$out .= "\t\t'database' => '{$database}',\n";

		if (!empty($schema)) {
			$out .= "\t\t'schema' => '{$schema}',\n";
		}

		if ($prefix) {
			$out .= "\t\t'prefix' => '{$prefix}',\n";
		}

		$out .= "\t\t'encoding' => 'utf8',\n";

		$out .= "\t);\n";

		$out .= "}\n";
		$filename = $this->path . 'database.php';
		$this->createFile($filename, $out);

		$conn = new mysqli($host, $login, $password);
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}

		$this->insertDefaultValues($conn, $database);

		return;
	}

	public function insertDefaultValues($conn, $database){
		$conn->query("
			CREATE DATABASE {$database}"
		);
		$conn->query("
			USE {$database}
		");
		$file = APP. DS . 'Config' . DS . 'Schema' . DS .'sql' . DS .'default_database.sql';

		$this->run_sql_file($file, $conn);

		return;
	}

	function run_sql_file($location, $conn){
		function file_get_contents_utf8($fn) {
			$content = file_get_contents($fn);
			 return mb_convert_encoding($content, 'ISO-8859-1',
				 mb_detect_encoding($content, 'UTF-8, ISO-8859-1', true));
	   	}
		//load file
		$commands = file_get_contents_utf8($location);

		//convert to array
		$commands = explode("/**/---newlinenow---", $commands);
		//run commands
		$total = 0;
		$success = 0;

		foreach($commands as $key => $command){
			if(trim($command)){
				if($conn->query($command) == false){
					print_r(mysqli_error($conn));exit;
				}
				$total += 1;
			}
		}
		//return number of successful queries and total number of queries found
		return array(
			"success" => $success,
			"total" => $total
		);
	}

	// Here's a startsWith function
	function startsWith($haystack, $needle){
		$length = strlen($needle);
		return (substr($haystack, 0, $length) === $needle);
	}

	public function createFile($path, $contents) {
		$path = str_replace(DS . DS, DS, $path);

		$File = new File($path, true);
		if ($File->exists() && $File->writable()) {
			$data = $File->prepare($contents);
			$File->write($data);
			return true;
		}
		return false;
	}
}
