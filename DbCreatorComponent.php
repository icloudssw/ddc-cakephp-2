<?php

require_once('src/DatabaseCreator.php');
App::uses('ConnectionManager', 'Model');

class DbCreatorComponent extends Component{

	public function __construct(){
		$this->DatabaseCreator = new DatabaseCreator();
	}

	public function bake($newConfigs = null) {
		$actualConfigs = $this->getConfigs();

		$this->DatabaseCreator->create($actualConfigs, $newConfigs);
	}

	public function getConfigs() {
		$configs = ConnectionManager::enumConnectionObjects();
		return $configs;
	}
}
