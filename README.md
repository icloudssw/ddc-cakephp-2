
# Dynamic Database Creator

Use it to generate databases dynamically


## How to install

1. Add this project like submodule into app/Controller/Component:

```shell

cd app/Controller/Component

git submodule add https://icloudssw@bitbucket.org/icloudssw/ddc-cakephp-2.git DynamicDatabaseCreator

```


2. Add this code on each controller that you want use the component methods or in AppController to include in all controllers.


```php
/* Import from DynamicDatabaseCreator, use it in AppController only */
App::build(array(
    'Controller/Component' => array(
        APP.'Controller'.DS.'Component'.DS.'DynamicDatabaseCreator'.DS,
    )
), App::APPEND);

/* Use it where you want use the component */
public $components = ['DbCreator'];

```

3. If are you using pipelines, please add the following code to update submodules in deploy:

```yml
	# Add before git push or git ftp push:
	- git submodule update --init --recursive
```


## How to use


1. Create a folder into app/Config/Schema named by "sql" and add the SQL archive named by "default_database.sql"

2. Call the function bake

```php

// just set the database name by parameter
$this->DbCreator->bake(['name' => 'database_name']);

```

4. IMPORTANT!
Use the comment ---newlinenow--- between each sql command on your sql file
